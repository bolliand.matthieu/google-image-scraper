from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import requests
import shutil
import os
import numpy as np
import argparse


#TODO change with your parameters
output_dir = ''
iterate = 100
chromedriver_path = ""
input = ""


def save_img(img,i):
    try:
        print(img.split("/")[-1])
        filename = str(i)+'.jpg'
        response = requests.get(img,stream=True)
        image_path = os.path.join(output_dir, filename)
        with open(image_path, 'wb') as file:
            shutil.copyfileobj(response.raw, file)
        return 1
    except Exception:
        return 0

    
def get_images():
    
    number_of_images = 0
    
    driver = webdriver.Chrome(chromedriver_path)
    
    driver.get("https://www.google.com/advanced_image_search")
    time.sleep(1) # page need to be load, TODO change all time.sleep if your internet connection is slow to avoid any errors
    
    search = driver.find_element_by_xpath('//*[@id="xX4UFf"]')
    search.send_keys(input)
    
    #select min size
    time.sleep(np.random.ranf())
    driver.find_element_by_xpath("//*[@id=\"imgsz_button\"]").click()
    time.sleep(np.random.ranf())
    #change min size (#TODO edit this part for another size that min 800x600)
    driver.find_element_by_xpath("//*[@id=\":7a\"]/div").click()
    time.sleep(np.random.ranf())
    
    
    #-------------------------
    #TODO add other parameter with this syntax just edit the XPATH
    #driver.find_element_by_xpath("//*[@id=\"imgtype_button\"]").click()
    #time.sleep(np.random.ranf())
    #driver.find_element_by_xpath("//*[@id=\":7u\"]/div").click()
    #time.sleep(np.random.ranf())
    #-------------------------
    
    #select file type
    driver.find_element_by_xpath("//*[@id=\"as_filetype_button\"]").click()
    time.sleep(np.random.ranf())
    driver.find_element_by_xpath("//*[@id=\":6s\"]").click()
    time.sleep(np.random.ranf())
    
    driver.find_element_by_xpath("//*[@id=\"s1zaZb\"]/div[5]/div[10]/div[2]/input[2]").click()
    time.sleep(2)
    
    #go all the way down
    for _ in range(50):
        driver.find_element_by_xpath('//*[@id="yDmH0d"]').send_keys(Keys.SPACE)
        time.sleep(0.5 + np.random.ranf() / 4)
    
    #click on show more
    driver.find_element_by_xpath("//*[@id=\"islmp\"]/div/div/div/div[1]/div[2]/div[2]/input").click()
    time.sleep(0.5 + np.random.ranf() / 4)
    
    #go back up
    driver.find_element_by_tag_name('html').send_keys(Keys.PAGE_UP)
    driver.execute_script("window.scrollTo(0, 220)")
    time.sleep(0.5 + np.random.ranf() / 4)
    
    for j in range (1,iterate+1):
        try:
            driver.find_element_by_xpath('//div//div//div//div//div//div//div//div//div//div['+str(j)+']//a[1]//div[1]//img[1]').click()
            time.sleep(0.5 + np.random.ranf() / 4)
            img = driver.find_element_by_xpath('//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div[2]/div[1]/a/img').get_attribute("src")
            #print(img)
            number_of_images +=  save_img(img,j)
        except Exception:
            pass
    
    time.sleep(2)
    driver.quit()
    
    print(number_of_images, "images were successfully save out of", iterate)
    
            
get_images()
